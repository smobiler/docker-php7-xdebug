<?php

/*

Create DB Sanitize script
It should be similar to DB prepare STAG with the following additions:

1. reset all admin user sessions  -- not implemented
2. reset all frontend customer frontend sessions  -- not implemented
3. change all admin user emails to following: 'carbon381+<user_id>@neklo.com'
4. change all frontend user emails to following: 'carbon380+<user_id>@neklo.com'
5. reset all admin user passwords to '12345678'
6. reset all frontend user passwords to '12345678'
7. change all coupon codes to: 'coupon<coupon_id>'
8. change all giftcards to: 'giftcard<giftcard_id>'
9. reset all FTP credentials in configs
something else ?

*/


require_once 'abstract.php';

class Neklo_Shell_DbPrepare extends Mage_Shell_Abstract
{
    protected $_config = null;

    protected $_writeConnection = null;

    protected $_allowedEmailDomainList = array(
        'neklo.com',
        'neklo-example.com',
    );

    public function __construct()
    {
        parent::__construct();
        /* @var array $config */
        if ($this->_isConfigExists()) {
            require_once 'dbprepare' . DS . $this->_getConfigFileParam() . '.php';
        }
        $this->_setConfig($config);
    }

    public function run()
    {
        if ($this->_getConfig() === null) {
            echo Mage::helper('adminhtml')->__("Config file 'shell/dbprepare/{$this->_getConfigFileParam()}.php' doesn't exist.") . PHP_EOL;
            return null;
        }

        $deepClean = false;
        if (count($this->_getConfigSection('deep_clean')) > 0) {
            $deepClean = true;
        }

        if (!$deepClean) {
            $this->_changeConfigValues();
            echo Mage::helper('adminhtml')->__('Config has been updated.') . PHP_EOL;
            //$this->_changeEmails();
            echo Mage::helper('adminhtml')->__('Email addresses have been updated.') . PHP_EOL;
            $this->_updateAdminUsers();
            echo Mage::helper('adminhtml')->__('Admin users have been updated.') . PHP_EOL;

            $env = trim($this->_getConfigFileParam());
            if (!in_array($env, array('dev', 'staging', 'playground'))) {

                $this->changeDefiner();
                echo Mage::helper('adminhtml')->__('Definer has been updated.') . "\n";
            }

        } else {
            $deepCleanConfig = $this->_getConfigSection('deep_clean');
            echo Mage::helper('adminhtml')->__('Additional sanitizing scenario.') . PHP_EOL;

            $this->_changeConfigValuesClean();
            $this->_changeConfigValues();
            echo Mage::helper('adminhtml')->__('Config has been updated.') . PHP_EOL;

            $this->_changeEmailsClean();
            echo Mage::helper('adminhtml')->__('Customers email addresses were updated.') . PHP_EOL;
            $this->_updateCustomerPasswords($deepCleanConfig);
            echo Mage::helper('adminhtml')->__('Customers passwords were updated.') . PHP_EOL;
            $this->_updateAdminUsersClean($deepCleanConfig);
            echo Mage::helper('adminhtml')->__('Admin users have been updated.') . PHP_EOL;
            $this->_updateCoupons();
            echo Mage::helper('adminhtml')->__('Coupons were updated.') . PHP_EOL;

            $this->_updateGiftCards();
            $this->_updateGiftcardsClean();
            $this->changeGiftCardsOrderItems();
            echo Mage::helper('adminhtml')->__('GiftCards were updated.') . PHP_EOL;

            $this->_updateShipments();
            echo Mage::helper('adminhtml')->__('Tracking numbers were updated.') . PHP_EOL;
            $this->_updateCC();
            echo Mage::helper('adminhtml')->__('Credit Card were updated.') . PHP_EOL;
            $this->_update3rdParty();
            echo Mage::helper('adminhtml')->__('3rd party tables were updated.') . PHP_EOL;
            $this->_updateApi();
            echo Mage::helper('adminhtml')->__('Api credentials were updated.') . PHP_EOL;

            $this->updateSalesTables();
            $this->changeCustomTables();
            $this->updateNewsletterSubscribers();

            $this->dropUnusedTables();
            $this->truncateTables();

            echo Mage::helper('adminhtml')->__('Custom tables have been updated.') . PHP_EOL;


            Mage::app()->cleanCache();
            Mage::app()->getCacheInstance()->flush();
            echo Mage::helper('adminhtml')->__('The cache storage has been flushed.') . PHP_EOL;
        }

        return $this;

    }

    protected function _getConfigFileParam()
    {
        return $this->getArg('env');
    }

    protected function _isConfigExists()
    {
        $currentEnv = $this->_getConfigFileParam();
        if (!file_exists(BP . DS . 'shell' . DS . 'dbprepare' . DS . $currentEnv . '.php')) {
            return false;
        }
        return true;
    }

    protected function _changeConfigValues()
    {
        $configValueList = $this->_getConfigSection('core_config');
        foreach ($configValueList as $scope => $valueList) {
            switch ($scope) {
                case 'default':
                    foreach ($valueList as $path => $value) {
                        $this->_updateConfigValue($path, $value);
                    }
                    break;
                case 'stores':
                case 'websites':
                    foreach ($valueList as $scopeId => $configValueList) {
                        foreach ($configValueList as $path => $value) {
                            $this->_updateConfigValue($path, $value, $scope, $scopeId);
                        }
                    }
                    break;
            }
        }
    }

    // Reset some config values
    protected function _changeConfigValuesClean()
    {
        $reset = $this->_getConfigSection('config_clean');

        $table = $this->_getTableName('core/config_data');
        foreach ($reset as $path) {

            $where = array(
                new Zend_Db_Expr("`path` like '{$path}'"),
            );
            $this->_getWriteConnection()->update($table, array('value' => '',), $where);
        }

        return $this;
    }

    /**
     *  Change emails
     *
     * @return $this
     * @throws Exception
     */
    protected function _changeEmails()
    {
        $emailValueList = $this->_getConfigSection('customer_email');

        $tablesToUpdate = array();

        if (array_key_exists('customer', $emailValueList) && $emailValueList['customer']) {
            $customerTable = $this->_getTableName('customer/entity');
            $tablesToUpdate[$customerTable] = 'email';
        }
        if (array_key_exists('newsletter', $emailValueList) && $emailValueList['newsletter']) {

            $subscriberTable = $this->_getTableName('newsletter/subscriber');
            $tablesToUpdate[$subscriberTable] = 'subscriber_email';
        }
        if (array_key_exists('sales', $emailValueList) && $emailValueList['sales']) {
            // Orders
            $orderTable = $this->_getTableName('sales/order');
            $tablesToUpdate[$orderTable] = 'customer_email';

            // Quotes
            $quoteTable = $this->_getTableName('sales/quote');
            $tablesToUpdate[$quoteTable] = 'customer_email';

            // Order Addresses
            $orderAddressTable = $this->_getTableName('sales/order_address');
            $tablesToUpdate[$orderAddressTable] = 'email';

            // Quote Addresses
            $quoteAddressTable = $this->_getTableName('sales/quote_address');
            $tablesToUpdate[$quoteAddressTable] = 'email';
        }


        // update tables
        foreach ($tablesToUpdate as $tableName => $column) {
            $this->_changeEmailColumn($tableName, $column);
        }

        return $this;
    }

    /**
     * Change email column according to the new pattern
     *
     * @param $tableName   The table to update.
     * @param $column
     * @return int   The number of affected rows.
     * @throws Zend_Db_Adapter_Exception
     */
    protected function _changeEmailColumn($tableName, $column)
    {
        //original email: bdroutman@yahoo.com
        //scrambled email: test_customer+bdroutman_at_yahoo.com@carbon38.com

        $update = array(
            $column => new Zend_Db_Expr("CONCAT('test_customer+', REPLACE(`{$column}`, '@', '_at_'), '@carbon38.com')")
        );

        $where = array();

        //skip allowed domains
        if (is_array($this->_allowedEmailDomainList) && count($this->_allowedEmailDomainList)) {
            foreach ($this->_allowedEmailDomainList as $allowedDomain) {
                $where[] = new Zend_Db_Expr("`{$column}` NOT LIKE '%@{$allowedDomain}'");
            }
        }

        //skip already updated emails
        $where[] = new Zend_Db_Expr("`{$column}` NOT LIKE 'test_customer+%@carbon38.com'");

        // update table rows
        return $this->_getWriteConnection()->update($tableName, $update, $where);
    }

    protected function _updateAdminUsers()
    {
        $adminUserList = $this->_getConfigSection('admin_user');
        foreach ($adminUserList as $userData) {
            if (!array_key_exists('username', $userData)) {
                continue;
            }
            if (!array_key_exists('password', $userData)) {
                continue;
            }
            $this->_updateAdminUser($userData['username'], $userData['password']);
        }
    }

    protected function _updateConfigValue($path, $value, $scope = 'default', $scopeId = 0)
    {
        $value = $this->_prepareConfigValue($path, $value);
        /* @var Mage_Core_Model_Config $configModel */
        $configModel = Mage::getModel('core/config');
        $configModel->saveConfig($path, $value, $scope, $scopeId);
        return $this;
    }

    protected function _prepareConfigValue($path, $value)
    {
        $fullPath = 'stores/' . Mage::app()->getStore()->getCode() . '/' . $path;
        $node = Mage::getConfig()->getNode($fullPath);
        if (!$node) {
            $node = Mage::getConfig()->getNode('default/' . $path);
        }
        if ($node && $node->getAttribute('backend_model') === 'adminhtml/system_config_backend_encrypted') {
            $value = Mage::helper('core')->encrypt($value);
        }
        return $value;
    }

    protected function _updateAdminUser($username, $password)
    {
        $adminUser = Mage::getModel('admin/user')->loadByUsername($username);
        if ($adminUser->getId()) {
            $adminUser
                ->setPassword($password)
                ->setIsActive(1)
                ->save();
        }else{
            $this->_createAdminUser($username, $password);
        }
        return $this;
    }

    protected function _createAdminUser($username, $password)
    {
        try {
            $user = Mage::getModel('admin/user')->setData(
                array(
                    'username' => $username,
                    'firstname' => $username,
                    'lastname' => $username,
                    'email' => 'autotest+235@neklo.com',
                    'password' => $password,
                    'is_active' => 1
                ))->save();

        } catch (Exception $e) {
            echo $e->getMessage();
        }

        try {
            $user->setRoleIds(array(1))//Administrator role id is 1
            ->setRoleUserId($user->getUserId())->saveRelations();

        } catch (Exception $e) {
            echo $e->getMessage();
        }
        return $this;
    }

    protected function changeDefiner()
    {
        $connConfig = Mage::getConfig()->getResourceConnectionConfig('core_write')->asArray();

        $user = $database = null;
        if (isset($connConfig)) {
            $database = $connConfig['dbname'];
        }

        if (isset($connConfig['username'])) {
            $user = $connConfig['username'];
        }

        if ($user && $database) {
            $query = "UPDATE mysql.proc p SET definer = '{$user}@%' WHERE definer='carbon38@%' AND Db = '{$database}';";
            $this->_getWriteConnection()->query($query);
        }
    }

    protected function _installMysqlProcedure()
    {

        $functionCreateQuery = "CREATE FUNCTION `bd_camel`(
        in_name VARCHAR(255)
        ) RETURNS varchar(255) CHARSET latin1
            NO SQL
            DETERMINISTIC
        BEGIN
        DECLARE done BOOLEAN;
        DECLARE remainder VARCHAR(255) DEFAULT concat(in_name, ' ');
        DECLARE cameled_name VARCHAR(255) DEFAULT '';
        DECLARE name_part VARCHAR(255);
        WHILE length(remainder) > 0 DO
        SET name_part := SubStr(remainder, 1, Instr(remainder, ' '));
        SET name_part := concat(upper(SubStr(name_part, 1, 1)), lower(SubStr(name_part, 2)));
        SET cameled_name := concat(cameled_name, name_part);
        SET remainder := SubStr(remainder, Instr(remainder, ' ') +1);
        END WHILE;
        RETURN cameled_name;
        END";

        $this->_writeConnection->query($functionCreateQuery);
    }

    protected function _getWriteConnection()
    {
        if ($this->_writeConnection === null) {
            $resource = Mage::getSingleton('core/resource');
            $this->_writeConnection = $resource->getConnection('core_write');
        }
        return $this->_writeConnection;
    }

    protected function _getDatabaseName()
    {
        return (string)Mage::getConfig()->getNode('global/resources/default_setup/connection/dbname');
    }

    /**
     * @param string $modelEntity
     *
     * @return null|string
     */
    protected function _getTableName($modelEntity)
    {
        try {
            /* @var Mage_Core_Model_Resource $resource */
            $resource = Mage::getSingleton('core/resource');
            $tableName = $resource->getTableName($modelEntity);
            return $tableName;
        } catch (Exception $e) {
            return null;
        }
    }

    protected function _setConfig($config)
    {
        $this->_config = $config;
        return $this;
    }

    protected function _getConfig()
    {
        return $this->_config;
    }

    protected function _getConfigSection($section)
    {
        $config = $this->_getConfig();
        if (!array_key_exists($section, $config)) {
            return array();
        }
        if (!is_array($config[$section])) {
            return array();
        }
        if (!$config[$section]) {
            return array();
        }
        return $config[$section];
    }

    protected function _changeEmailsClean()
    {
        $emailValueList = $this->_getConfigSection('customer_email');
        if (!is_array($emailValueList)) {
            return;
        }
        foreach ($emailValueList as $tableConfigValue => $replaceParams) {
            $tableName = $this->_getTableName($tableConfigValue);
            if ($tableName === null) {
                continue;
            }
            $emailFieldName = $replaceParams[0];
            $idFieldName = $replaceParams[1];

            $query = "UPDATE `{$tableName}` SET `{$emailFieldName}` = CONCAT('carbon380','+',$idFieldName,'@neklo.com')";
            $this->_getWriteConnection()->query($query);
        }
    }

    protected function _updateCustomerPasswords($deepCleanConfig = null)
    {
        $customersPassword = 12345678;
        if (is_array($deepCleanConfig) && $deepCleanConfig['customer_password']) {
            $customersPassword = $deepCleanConfig['customer_password'];
        }

        $hash = md5("SC" . $customersPassword) . ":SC";


        $customerEntityVarcharTable = $this->_getTableName('customer_entity_varchar');
        $attributeTable = $this->_getTableName('eav/attribute');

        $update = array(
            'value' => $hash,
        );

        $where = array();
        $where[] = new Zend_Db_Expr("attribute_id IN (SELECT attribute_id FROM {$attributeTable}
                       WHERE attribute_code = 'password_hash'
                         AND entity_type_id = 1 )");

        $this->_getWriteConnection()->update($customerEntityVarcharTable, $update, $where);
    }


    protected function _updateAdminUsersClean($deepCleanConfig = null)
    {
        $adminPassword = 12345678;
        if (is_array($deepCleanConfig) && $deepCleanConfig['admin_password']) {
            $adminPassword = $deepCleanConfig['admin_password'];
        }
        $adminCollection = Mage::getModel('admin/user')->getCollection();

        foreach ($adminCollection as $admin) {

            $id = $admin->getId();
            $email = 'carbon381+' . $id . '@neklo.com';

            $admin->setPassword($adminPassword);
            $admin->setEmail($email);
            $admin->save();
        }
        return $this;
    }

    protected function _updateCoupons()
    {

        $updates = $this->_getConfigSection('coupons_clean');

        foreach ($updates as $entity => $changes) {
            $this->_update($entity, $changes);
        }

        $couponValueList = $this->_getConfigSection('coupons');
        if (!is_array($couponValueList)) {
            return;
        }
        foreach ($couponValueList as $tableConfigValue => $replaceParams) {
            $tableName = $this->_getTableName($tableConfigValue);
            if ($tableName === null) {
                continue;
            }
            $couponFieldName = $replaceParams[0];
            $replaceWith = $replaceParams[1];
            $query = "UPDATE `{$tableName}` SET `{$couponFieldName}` = $replaceWith";
            $this->_getWriteConnection()->query($query);
        }

    }

    protected function _updateGiftcards()
    {
        $giftcardsValueList = $this->_getConfigSection('giftcards');
        if (!is_array($giftcardsValueList)) {
            return;
        }
        foreach ($giftcardsValueList as $tableConfigValue => $replaceParams) {
            $tableName = $this->_getTableName($tableConfigValue);
            if ($tableName === null) {
                continue;
            }
            $giftCardFieldName = $replaceParams[0];
            $replaceWith = $replaceParams[1];
            $query = "UPDATE `{$tableName}` SET `{$giftCardFieldName}` = $replaceWith";
            $this->_getWriteConnection()->query($query);
        }
    }

    /**
     *  Change all giftcards to: 'giftcard<giftcard_id>'
     */
    protected function _updateGiftcardsClean()
    {
        // update code

        $changes = array(
            'update' => array(
                'card_code' => "CONCAT('giftcard',`card_id`)",
            ),
        );

        $this->_update('giftcards/giftcards', $changes);


        // change email
        $changes = array(
            'update' => array(
                'mail_to_email' => "CONCAT('carbon380+','giftcard',`card_id`,'@neklo.com')",
            ),
            'where' => array(
                "`mail_to_email` != ''"
            )
        );

        $this->_update('giftcards/giftcards', $changes);
    }


    public function changeGiftCardsOrderItems()
    {
        $orderItemCollection = Mage::getModel('sales/order_item')->getCollection()
            ->addFieldToFilter('product_type', array('in' => array('giftcard', 'giftcards')));

        $orderItemCollection->getSelect()
            ->reset(Zend_Db_Select::COLUMNS)
            ->columns(array('item_id', 'product_options'));

        foreach ($orderItemCollection as $item) {

            $itemId = $item->getId();
            $options = unserialize($item->getData('product_options'));
            $needToSave = 0;

            if (isset($options['info_buyRequest']['mail_to_email'])) {
                $options['info_buyRequest']['mail_to_email'] = 'carbon380+order-item' . $itemId . '@neklo.com';
                $needToSave = 1;
            }

            if (isset($options['info_buyRequest']['giftcard_sender_email'])) {
                $options['info_buyRequest']['giftcard_sender_email'] = 'carbon380+order-item' . $itemId . '@neklo.com';
                $needToSave = 1;
            }
            if (isset($options['info_buyRequest']['giftcard_recipient_email'])) {
                $options['info_buyRequest']['giftcard_recipient_email'] = 'carbon380+order-item' . $itemId . '@neklo.com';
                $needToSave = 1;
            }
            if (isset($options['giftcard_sender_email'])) {
                $options['giftcard_sender_email'] = 'carbon380+order-item' . $itemId . '@neklo.com';
                $needToSave = 1;
            }
            if (isset($options['giftcard_recipient_email'])) {
                $options['giftcard_recipient_email'] = 'carbon380+order-item' . $itemId . '@neklo.com';
                $needToSave = 1;
            }
            if (isset($options['giftcard_created_codes'])) {
                foreach ($options['giftcard_created_codes'] as $key => $code) {
                    $options['giftcard_created_codes'][$key] = md5($code);
                }
                $needToSave = 1;
            }
            if ($needToSave == 1) {
                $item->setData('product_options', serialize($options));
                $item->save();
            }
        }


        $quoteItemOptionCollection = Mage::getModel('sales/quote_item_option')
            ->getCollection()
            ->addFieldToFilter('code', array('eq' => array('mail_to_email')));

        foreach($quoteItemOptionCollection as $option){

            $email = 'carbon380+quote-item-option' . $option->getId() . '@neklo.com';
            $option->setValue($email)               ->save();
        }

        $quoteItemOptionCollection = Mage::getModel('sales/quote_item_option')
            ->getCollection()
            ->addFieldToFilter('value', array('like' => '%mail_to_email%'));

        foreach ($quoteItemOptionCollection as $option) {
            $options = unserialize($option->getValue());
            if (isset($options['mail_to_email'])) {
                $email = 'carbon380+quote-item-option' . $option->getId() . '@neklo.com';
                $options['mail_to_email'] = $email;
                $option->setValue(serialize($options))->save();
            }
        }

        return $this;
    }


    protected function _updateShipments()
    {
        $tableName = $this->_getTableName('sales/shipment_track');
        $query = "UPDATE `$tableName` SET `track_number` = MD5(CONCAT(RAND(),`track_number`))";
        $this->_getWriteConnection()->query($query);
    }

    protected function _updateCC()
    {
        $tableName = $this->_getTableName('sales/quote_payment');
        $query = "UPDATE `$tableName` SET `cc_last4` = '1234',`cc_exp_year` = '2020',`cc_exp_month` = '12' where `cc_type` IS NOT NULL";
        $this->_getWriteConnection()->query($query);
    }

    protected function _update3rdParty()
    {
        $list = $this->_getConfigSection('3rd_party_tables');
        if (!is_array($list)) {
            return;
        }
        foreach ($list as $entity => $changes) {
            $this->_update($entity, $changes);
        }
    }

    protected function _updateApi()
    {
        $list = $this->_getConfigSection('api');
        if (!is_array($list)) {
            return;
        }
        foreach ($list as $entity => $changes) {
            $this->_update($entity, $changes);
        }
    }

    /**
     *  Change data in 'sales_' tables
     *
     * @return $this
     */
    public function updateSalesTables()
    {
        $updates = $this->_getConfigSection('sales_tables_clean');

        foreach ($updates as $changes) {
            $entity = $changes['entity'];
            $this->_update($entity, $changes);
        }

        return $this;
    }

    public function  truncateTables()
    {
        $tables = $this->_getConfigSection('truncate_tables');

        foreach ($tables as $table) {
            $this->_getWriteConnection()->query("TRUNCATE TABLE `{$table}`");
        }
    }

    /**
     *  Remove unused tables
     *
     * @return $this
     */
    public function dropUnusedTables()
    {
        $tables = $this->_getConfigSection('drop_tables');

        foreach ($tables as $table) {
            $this->_getWriteConnection()->query('DROP TABLE IF EXISTS `' . $table . '`');
        }

        return $this;
    }


    /**
     *  Change data in custom tables
     *
     * @return $this
     */
    public function changeCustomTables()
    {

        $updates = $this->_getConfigSection('change_custom_tables');

        foreach ($updates as $entity => $changes) {
            $this->_update($entity, $changes);
        }

        return $this;
    }

    /**
     *  Change newsletter subscribers
     * @return $this
     */
    public function updateNewsletterSubscribers()
    {
        $updates = $this->_getConfigSection('subscribers');;

        foreach ($updates as $entity => $changes) {
            $this->_update($entity, $changes);
        }

        return $this;
    }


    protected function _update($entity, $changes)
    {
        $table = $this->_getTableName($entity);

        $update = array();
        foreach ($changes['update'] as $key => $value) {
            $update[$key] = new Zend_Db_Expr($value);
        }
        $where = isset($changes['where']) ? $changes['where'] : null;

        try {
            $this->_getWriteConnection()->update($table, $update, $where);
        } catch (Exception $e) {
            var_dump($e);
        }


        return $this;
    }
}

try {
    $shell = new Neklo_Shell_DbPrepare();
    $shell->run();
} catch (Exception $e) {
    echo $e->getMessage() . PHP_EOL . PHP_EOL .
    $e->getTraceAsString() . PHP_EOL;
    exit(1); //stop build
}
