#!/bin/bash

DB_DUMP_FILE='c38_stag_new.sql.gz'
#DB_DUMP_SRC='http://neklo-backups.s3.amazonaws.com/extradbdumps/c38_stag_new.sql.gz'
DB_DUMP_SRC='http://neklo-backups.s3.amazonaws.com/extradbdumps/db_dev.sql.gz'
DB_DUMP_FILE_SQL='dump.sql'

#set name new DB
DB_NAME='carbon1'

LOCAL_HOST='http://carbon.loc'
DEV_PATH_PROJ="/var/www/html"

DB_NAME_CURRENT='carbon' #default
SESSION_LIFE_TIME='909090'
COMPOSER='~/bin/composer.phar'
MYSQL_PATH=''
DEV_FILE_LOCAL='/var/www/update_db/local.xml'
DEV_FILE_DEV='/var/www/update_db/dev.php'
DEV_FILE_DBPREPARE='/var/www/update_db/dbprepare.php'



DEV_XML_LOCAL='<dbname><![CDATA['${DB_NAME_CURRENT}']]></dbname>'
DEV_XML_LOCAL_REPLACE_TO='<dbname><![CDATA['${DB_NAME}']]></dbname>'


function notify {
    echo "display notification \"$*\""
}

echo '*** [Download DB dump] ******************************************************'
rm -f $DB_DUMP_FILE
wget $DB_DUMP_SRC -O $DB_DUMP_FILE
notify 'DB download complete'

echo '*** [Create db] *****************************************************'
${MYSQL_PATH}mysql -f -u root -proot -e  "DROP DATABASE IF EXISTS ${DB_NAME}; CREATE DATABASE ${DB_NAME};"

echo '*** [Importing DB] **********************************************************'
pv -cN source $DB_DUMP_FILE | gunzip -c | pv -cN ${MYSQL_PATH}mysql  | ${MYSQL_PATH}mysql  -f -u root -proot ${DB_NAME}
#pv -cN ${MYSQL_PATH}mysql | ${MYSQL_PATH}mysql -f -u root -proot ${DB_NAME} < ${DB_DUMP_FILE_SQL}
notify 'Import DB complete'

echo "*** [Applying local configuration] ***** *************************************"
echo 'update local xml'

cd $DEV_PATH_PROJ

cp $DEV_FILE_LOCAL $DEV_PATH_PROJ/app/etc/
php -r "file_put_contents('"${DEV_PATH_PROJ}"/app/etc/local.xml',str_replace('"${DEV_XML_LOCAL}"','"${DEV_XML_LOCAL_REPLACE_TO}"', file_get_contents('"${DEV_PATH_PROJ}"/app/etc/local.xml')));"
cp $DEV_FILE_DEV $DEV_PATH_PROJ/shell/dbprepare/
cp $DEV_FILE_DBPREPARE ${DEV_PATH_PROJ}/shell/

echo '*** [Clear cache] ***********************************************************'
n98-magerun.phar cache:clean; 
n98-magerun.phar cache:flush; 
rm -r ${DEV_PATH_PROJ}/var/cache/mage*


echo '*** [Update code] ***********************************************************'
cd $DEV_PATH_PROJ
n98-magerun.phar admin:user:create smobiler smobiler@yandex.ru 123456 test test
php shell/dbprepare.php --env dev

echo '*** [Clear cache] ***********************************************************'
n98-magerun.phar cache:clean; 
n98-magerun.phar cache:flush; 
rm -r ${DEV_PATH_PROJ}/var/cache/mage*

notify 'display notification "Project update complete" with title "DB updater"'
