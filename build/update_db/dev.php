<?php
$config = array(
    // Change Config Values
    'core_config' => array(
        'default' => array(
            // Base Urls
            'web/unsecure/base_url' => 'http://carbon.loc/',
            'web/secure/base_url' => 'http://carbon.loc/',

            // CDN
            'web/unsecure/base_skin_url' => '{{unsecure_base_url}}skin/',
            'web/unsecure/base_media_url' => '{{unsecure_base_url}}media/',
            'web/unsecure/base_js_url' => '{{unsecure_base_url}}js/',
            'web/secure/base_skin_url' => '{{secure_base_url}}skin/',
            'web/secure/base_media_url' => '{{secure_base_url}}media/',
            'web/secure/base_js_url' => '{{secure_base_url}}js/',

            // MageMonkey
            'monkey/general/active' => 0,
            'mandrill/general/active' => 0,
            'ebizmarts_abandonedcart/general/active' => 0,
            'ebizmarts_autoresponder/general/active' => 0,

            // Payment Methods
            // Braintree
            'payment/braintree/environment' => 'sandbox',
            'payment/braintree/merchant_account_id' => 'neklo',
            'payment/braintree/merchant_id' => '595wp9mdpsy7m4x4',
            'payment/braintree/public_key' => 'y6y7mz75jvyj5hdx',
            'payment/braintree/private_key' => 'cebfbbd79e87c39c5cba16d69445a102',
            'payment/braintree/client_side_encryption_key' => 'MIIBCgKCAQEA23ZZXk6n3CpgIFGt3S14qtmbfa80zyU1tyty45ax8VLS82KRocv8mQl7RduSTYTCo+vZcDRFPAOCz2GMirqcZ+NQ5EJplaN+OzdXfDqLflojx0f7zggMlY2dQCXOfqm/3H2/TMLDkU9bL01IbSJLR9rzagyUjLBF4aUeF8kA16xS1SaUgryuM5jc5ty7uul2mF2QcQh7Lb+6IMD0rwJyT6ngfQ7Obh4cQLCIDmX1WAW0kIFn1WtuBfTznKNHXUsBZDKt51SpZUF0EAFG3o1IdyCrm1XRtEAZCCiXvD65fDigZQU65A5lD6Ihxztt7WHlhjYFHk19hdlPhweGmRLcVQIDAQAB',

            // Disable Google Analytics
            'google/analytics/active' => '1',
            'google/analytics/account' => 'UA-36740325-4',

            // Set Default Robots NOINDEX,NOFOLLOW
            'design/head/default_robots' => 'NOINDEX,NOFOLLOW',

            // Neklo Reports
            'nekreport/product_feed/ftp_path' => 'Dev/carbon38_product_feed',
            'nekreport/product_feed_weekly/ftp_path' => 'Dev/weekly_inventory_and_sales',
            'nekreport/criteo_product_feed/ftp_path' => 'Dev/carbon38_product_feed/criteo',
            'nekreport/product_feed_bbd/ftp_path' => 'Dev/carbon38_product_feed/bbd',
            'nekreport/google_review_feed/ftp_path' => 'Dev/carbon38_product_feed/googlereviews',

            'nekreport/newgistics_product_feed/ftp_host' => '',
            'nekreport/newgistics_product_feed/ftp_login' => '',
            'nekreport/newgistics_product_feed/ftp_path' => '',

            'nekreport/email_report/enable' => '0',
            'nekreport/email_report/email' => '',

            // Cookie
            'web/cookie/cookie_path' => '/',
            'web/cookie/cookie_domain' => 'carbon.loc',

            // AWS Cloud Search
            'neklo_awscloudsearch/general/endpoint' => 'search-devel-5txyxifdzlvv46t6vxb4aopudi.us-west-2.cloudsearch.amazonaws.com',
            'neklo_awscloudsearch/general/endpoint_doc' => 'doc-devel-5txyxifdzlvv46t6vxb4aopudi.us-west-2.cloudsearch.amazonaws.com',
            'neklo_awscloudsearch/general/active_domain' => 'devel',
            'neklo_awscloudsearch/general/datapack' => 'development',
            'neklo_awscloudsearch/general/suggester' => 'c38dev',
            'neklo_awscloudsearch/general/access_key' => 'AKIAJZPCDINL4UPW66HA',
            'neklo_awscloudsearch/general/client_key' => 'ggKYonoGf2EmSJfDChLtr0K8jP3O4pB+4muOajTu',

            // Sidecar Product Feed
            'neklo_sidecar/product_feed/enable' => '0',
            'neklo_sidecar/product_feed/ftp_host' => '',
            'neklo_sidecar/product_feed/ftp_login' => '',
            'neklo_sidecar/product_feed/ftp_password' => '',
            'neklo_sidecar/product_feed/ftp_path' => '',

            // LDAP
            'neklo_ldap/connection/filter' => '(memberof=cn=Carbon38MageDev,ou=Groups,dc=neklo,dc=com)',
            'neklo_ldap/general/is_enabled' => '1',
            'neklo_ldap/admin/role_id' => '1',

            // Magento Developer Mode
            'neklo_custom/developer/developer_mode' => 1,

            // Custom Admin Node
            'admin/url/use_custom'  => '1',
            'admin/url/custom'      => 'http://carbon.loc/',
            'web/url/redirect_to_base' => '0',

            // Google Tag Manager
            'google/gtm/containerid' => 'GTM-NBDPQT',

            // Store Emails
            'trans_email/ident_general/email' => 'katerina.khrolovich+c38admin@neklo.com',
            'trans_email/ident_sales/email' => 'katerina.khrolovich+c38admin@neklo.com',
            'trans_email/ident_support/email' => 'katerina.khrolovich+c38admin@neklo.com',
            'trans_email/ident_custom1/email' => 'katerina.khrolovich+c38admin@neklo.com',
            'trans_email/ident_custom2/email' => 'katerina.khrolovich+c38admin@neklo.com',

            'trans_email/ident_customer_care/email' => 'katerina.khrolovich+customer-care@neklo.com',

            'trans_email/ident_custom3/email' => 'katerina.khrolovich+custom3@neklo.com',
            'trans_email/ident_custom4/email' => 'katerina.khrolovich+custom4@neklo.com',
            'trans_email/ident_custom5/email' => 'katerina.khrolovich+custom5@neklo.com',

            // Sales Emails
            'sales_email/shipment/copy_to' => 'katerina.khrolovich+c38admin@neklo.com',
            'sales_email/giftcards_email/copy_to' => 'katerina.khrolovich+c38admin@neklo.com',
            'sales_email/first_order_with_price_rule/copy_to' => 'katerina.khrolovich+c38admin@neklo.com',
            'sales_email/order_with_price_rule/copy_to' => 'katerina.khrolovich+c38admin@neklo.com',

            // ATTRAQT
            'locayta/general/locayta_enabled' => 0, // disable by default
            'locayta/general/mode' => 2, // test mode

            //CJ
            'neklo_cj/ftp/host' => 'ftp.carbon38.com',
            'neklo_cj/ftp/user' => 'carbon38',
            'neklo_cj/ftp/pass' => 'swaf7tajeteT',
            'neklo_cj/ftp/path' => 'Dev/cj',

            //Neklo Newgistics
            'neklo_newgistics/general/ftp_path' => 'Dev/newgistics_product_feed',
            'neklo_newgistics/newgistics/api_url' => 'http://staging.api.atlastfulfillment.com/',
            'neklo_newgistics/manifests/ftp_upload_path' => 'Dev/',

            // Varnish settings
            'turpentine_varnish/servers/server_list' => '',

            'neklo_fpccrawler/server/host' => '',
            'neklo_fpccrawler/server/ip_1' => '',
            'neklo_fpccrawler/server/ip_2' => '',

            // Sift Science
            'neklo_siftscience/config/api_key' => 'a52c7d462d52e88b',
            'neklo_siftscience/config/js_key' => 'fd85720be4',

            // Responsys
            'neklo_responsys/responsys_api/table' => 'CONTACTS_LIST_DEV',
            'neklo_responsys/responsys_api/lifetime_pet' => 'LIFETIME_SUMMARY_PET_DEV',
            'neklo_responsys/responsys_api/order_pet' => 'ORDER_SUMMARY_PET_DEV',
            'neklo_responsys/responsys_api/unsub_preference_pet' => 'Unsubscribe_Preference_PET_DEV',
            'neklo_responsys/responsys_api/folder' => '!MasterDataDev',
            'neklo_responsys/responsys_api/product_supp_table' => 'PRODUCTS_SUPP_DEV',
            'neklo_responsys/responsys_api/order_supp_table' => 'ORDERS_SUPP_DEV',
            'neklo_responsys/responsys_api/order_items_supp_table' => 'ORDER_ITEMS_SUPP_DEV',
            'neklo_responsys/responsys_api/quote_table' => 'QUOTE_DEV',
            'neklo_responsys/responsys_api/quote_items_table' => 'QUOTE_ITEMS_DEV',
            'neklo_responsys/responsys_api/ctl_table' => 'CTLSUPP_DEV',
            'neklo_responsys/responsys_api/ymal_table' => 'YMALSUPP_DEV',

            // Payeezy
            'payment/payeezy/merchant_token' => 'fdoa-a480ce8951daa73262734cf102641994c1e55e7cdf4c02b6',
            'payment/payeezy/api_key' => 'y6pWAJNyJyjGv66IsVuWnklkKUPFbb0a',
            'payment/payeezy/api_secret' => '86fbae7030253af3cd15faef2a1f4b67353e41fb6799f576b5093ae52901e6f7',
            'payment/payeezy/js_secret' => 'js-6125e57ce5c46e10087a545b9e9d7354c23e1a1670d9e9c7',
            'payment/payeezy/transarmor_token' => 'NOIW',
            'payment/payeezy/environment' => 'sandbox',
        ),
        'stores' => array(

            '0' => array(
                // Cookie for admin node (backend)
                'web/cookie/cookie_domain' => 'carbon.loc',
                'web/unsecure/base_url' => 'http://carbon.loc/',
                'web/secure/base_url' => 'http://carbon.loc/',
            ),

            '1' => array(
                // CDN
                'web/unsecure/base_skin_url' => '{{unsecure_base_url}}skin/',
                'web/unsecure/base_media_url' => '{{unsecure_base_url}}media/',
                'web/unsecure/base_js_url' => '{{unsecure_base_url}}js/',
                'web/secure/base_skin_url' => '{{secure_base_url}}skin/',
                'web/secure/base_media_url' => '{{secure_base_url}}media/',
                'web/secure/base_js_url' => '{{secure_base_url}}js/',
            ),
            
            '2' => array(
                // CDN
                'web/unsecure/base_skin_url' => '{{unsecure_base_url}}skin/',
                'web/unsecure/base_media_url' => '{{unsecure_base_url}}media/',
                'web/unsecure/base_js_url' => '{{unsecure_base_url}}js/',
                'web/secure/base_skin_url' => '{{secure_base_url}}skin/',
                'web/secure/base_media_url' => '{{secure_base_url}}media/',
                'web/secure/base_js_url' => '{{secure_base_url}}js/',
            ),
        ),
    ),

    // Change Exists Customer Emails
    'customer_email' => array(
        // Change customer_entity emails
        'customer'   => true,

        // Change newsletter_subscriber emails
        'newsletter' => true,

        // Orders, Quote, Addresses
        'sales'      => true,

        //product alert
        'product_alert' => true,
    ),

    // Change admin user's password
    'admin_user' => array(
        array(
            'username' => 'admin',
            'password' => 'N3kl0.0001',
        ),
        array(
            'username' => 'autotest',
            'password' => 'aut0test.1',
        ),
    ),
);
